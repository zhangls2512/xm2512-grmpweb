'use strict'
exports.main = async (event) => {
  const tcb = require('@cloudbase/node-sdk')
  const app = tcb.init()
  const db = app.database()
  if (event.httpMethod != 'POST') {
    return {
      errCode: 1000,
      errMsg: '请求方法错误',
      errFix: '使用POST方法请求'
    }
  }
  try {
    const requestdata = JSON.parse(event.body)
    if (typeof (requestdata.accessToken) != 'string' && typeof (requestdata.accessKey) != 'string') {
      return {
        errCode: 1001,
        errMsg: '请求参数错误',
        errFix: '传递有效的accessToken或accessKey参数'
      }
    }
    if (typeof (requestdata.id) != 'string') {
      return {
        errCode: 1001,
        errMsg: '请求参数错误',
        errFix: '传递有效的id参数'
      }
    }
    let type = ''
    let code = ''
    if (requestdata.accessToken) {
      type = 'accesstoken'
      code = requestdata.accessToken
    } else {
      type = 'accesskey'
      code = requestdata.accessKey
    }
    const res = await app.callFunction({
      name: 'authCheck',
      data: {
        type: type,
        data: {
          code: code,
          requestIp: event.headers['x-real-ip']
        },
        permission: [],
        service: ['ssl'],
        apiName: 'ssl_getOrderInfo'
      }
    })
    if (res.result.errCode != 0) {
      return res.result
    } else {
      const orderres = await db.collection('sslorder').where({
        _id: requestdata.id,
        uid: res.result.account._id
      }).field({
        isNoticeOrderNearexpire: false,
        isNoticeCertificateNearexpire: false,
        orderUrl: false,
        uid: false
      }).get()
      if (orderres.data.length == 0) {
        return {
          errCode: 8000,
          errMsg: '订单不存在',
          errFix: '传递有效的id'
        }
      } else {
        let data = orderres.data[0]
        if (data.privateKey) {
          const privatekeyres = await app.getTempFileURL({
            fileList: [
              {
                fileID: data.privateKey,
                maxAge: 300
              }
            ]
          })
          data.privateKey = privatekeyres.fileList[0].tempFileURL
        }
        if (data.certificate.length > 0) {
          const promise = data.certificate.map(async (item) => {
            const certificateres = await app.getTempFileURL({
              fileList: [
                {
                  fileID: item.value,
                  maxAge: 300
                }
              ]
            })
            return {
              ...item,
              value: certificateres.fileList[0].tempFileURL
            }
          })
          data.certificate = await Promise.all(promise)
        }
        return {
          errCode: 0,
          errMsg: '成功',
          data: data
        }
      }
    }
  } catch {
    return {
      errCode: 5000,
      errMsg: '内部错误',
      errFix: '联系客服'
    }
  }
}