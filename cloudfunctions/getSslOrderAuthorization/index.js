'use strict'
exports.main = async (event) => {
  const tcb = require('@cloudbase/node-sdk')
  const acme = require('nodejs-acmeclient')
  const app = tcb.init()
  const auth = app.auth()
  const issdk = auth.getUserInfo().isAnonymous
  const db = app.database()
  let requestdata = ''
  let requestip = ''
  if (issdk) {
    requestdata = event
    requestip = auth.getClientIP()
  } else {
    requestdata = JSON.parse(event.body)
    requestip = event.headers['x-real-ip']
    if (event.httpMethod != 'POST') {
      return {
        errCode: 1000,
        errMsg: '请求方法错误',
        errFix: '使用POST方法请求'
      }
    }
  }
  try {
    if (typeof (requestdata.accessToken) != 'string' && typeof (requestdata.accessKey) != 'string') {
      return {
        errCode: 1001,
        errMsg: '请求参数错误',
        errFix: '传递有效的accessToken或accessKey参数'
      }
    }
    if (typeof (requestdata.id) != 'string') {
      return {
        errCode: 1001,
        errMsg: '请求参数错误',
        errFix: '传递有效的id参数'
      }
    }
    let type = ''
    let code = ''
    if (requestdata.accessToken) {
      type = 'accesstoken'
      code = requestdata.accessToken
    } else {
      type = 'accesskey'
      code = requestdata.accessKey
    }
    const res = await app.callFunction({
      name: 'authCheck',
      data: {
        type: type,
        data: {
          code: code,
          requestIp: requestip
        },
        permission: [],
        service: ['ssl'],
        apiName: 'ssl_getOrderAuthorization'
      }
    })
    if (res.result.errCode != 0) {
      return res.result
    } else {
      const orderres = await db.collection('sslorder').where({
        _id: requestdata.id,
        uid: res.result.account._id
      }).get()
      if (orderres.data.length == 0) {
        return {
          errCode: 8000,
          errMsg: '订单不存在',
          errFix: '传递有效的id'
        }
      } else {
        const userres = await db.collection('productuser').where({
          product: 'ssl',
          uid: res.result.account._id
        }).get()
        const accountkey = userres.data[0].accountKey[orderres.data[0].environmentType]
        const acmeorderres = await acme.api.getOrderInfo(orderres.data[0].orderUrl)
        let authorization = await acme.api.getOrderAuthorization(orderres.data[0].orderUrl)
        authorization = authorization.map((item, index) => ({
          ...item,
          url: acmeorderres.authorizations[index],
          identifier: item.identifier.value,
          expires: new Date(item.expires).getTime(),
          challenges: item.challenges.map(item => ({
            ...item,
            token: acme.api.getChallengeKeyAuthorization({
              challenge: item,
              accountKey: accountkey
            }),
            validated: new Date(item.validated).getTime()
          }))
        }))
        return {
          errCode: 0,
          errMsg: '成功',
          authorization: authorization
        }
      }
    }
  } catch {
    return {
      errCode: 5000,
      errMsg: '内部错误',
      errFix: '联系客服'
    }
  }
}